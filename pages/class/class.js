// pages/class/class.js
const app=getApp()
// 在页面中定义插屏广告
let interstitialAd = null
Page({

  /**
   * 页面的初始数据 
   */ 
  data: {
	  dataC:true,
	  //加载更多
      isLastPage: false,
      tips: '点击加载更多',
	  page:1,
	  limit:18, 
	  
	  //滑动隐藏显示
	  scrollTop:null,
	  showMenu:true,
	  menuAnim:null,
	  menuHeight:null,
	  //筛选点击
	  onclass:0,
	  ontype:0,
	  onaddress:0,
	  //
	  navList: [],
	  //筛选
	  type:6,
	  area:"",
	  // year:null,
	  sort:1,
	  
	  vClass:[
		      {name:"电影",id:"1"},
		      {name:"电视剧",id:"2"},
			  {name:"动漫",id:"4"},
		      {name:"综艺",id:"3"}
	  ],
	  // Sort:[
	  // 		  {name:"最新",id:"1"},
	  // 		  {name:"评分",id:"2"},
	  // 		  {name:"热度",id:"3"}
	  // ],
	  Address:[
		      {name:"全部地区",data:""},
	  		  {name:"大陆",data:"大陆"},
	  		  {name:"美国",data:"美国"},
	  		  {name:"英国",data:"英国"},
	  		  {name:"日本",data:"日本"},
	  		  {name:"韩国",data:"韩国"},
	  		  {name:"泰国",data:"泰国"},
	  		  {name:"法国",data:"法国"},
	  		  {name:"印度",data:"印度"},
			  {name:"加拿大",data:"加拿大"},
			  {name:"香港",data:"香港"},
			  {name:"台湾",data:"台湾"},
	  		  {name:"海外",data:"海外"},
	  		  {name:"其他",data:"其他"}
	  ],
	  
	  Type:[
	  		  {name:"动作片",id:"6"},
	  		  {name:"喜剧片",id:"7"},
	  		  {name:"爱情片",id:"8"},
	  		  {name:"科幻片",id:"9"},
	  		  {name:"恐怖片",id:"10"},
	  		  {name:"剧情片",id:"11"},
	  		  {name:"战争片",id:"12"}
	  ]
  },
  // 控制
	getDataC() {
		let that = this;
		wx.request({
			url: app.globalData.Url+"public/?service=App.DataControl.User",
			data:{
				dataC:1
			},
			success: (src) => {
				console.log(src.data.Data.content)
				if (src.data.Code === 200) {
					that.setData({
						dataC: src.data.Data.content
					})
				} else {
					console.log("错误");
				}
			}
		})
	},
  /* 
  影视
  */
 getList() {
	 // 发起请求
	         wx.showLoading({
	             title: '加载中',
	         })
 	let that = this;
 	// console.log(this.page)
 	wx.request({
 		url: app.globalData.Url+"public/?service=App.Mov.GetCategory",
		data:{
			type:that.data.type,
			area:that.data.area,
			year:"",
			sort:that.data.sort,
			page:that.data.page,
			limit:that.data.limit
		},
		success: (res) => {
		       if (res.statusCode!= 200 || res.data.Code != 200) {
		           that.showError()
		       } else {
		           wx.hideLoading()
		           var newData = {}
		           if (res.data.Data.length < that.data.limit) {
		               // 没有数据了，已经是最后一页
		               newData.isLastPage = true;
		               newData.tips = "没有更多了";
		           }
		           // 追加数据
		           newData.navList = this.data.navList.concat(res.data.Data)
		           this.setData(newData)
		       }
		   },
		   fail: () => {
		       that.showError()
		   },
		/*  */
 	})
 },
  /* 点击*/
  vclass:function(e){
	  
	  console.log(e)
	  this.setData({
		  onclass:e.currentTarget.dataset.index
	  })
	  let arr=Array();
	  let arr1=Array();
	  let k=Array();
	  let data=e.currentTarget.dataset.type; 
	  if(data==1){//dy
		  arr=[
			  {name:"动作片",id:"6"},
			  {name:"喜剧片",id:"7"},
			  {name:"爱情片",id:"8"},
			  {name:"科幻片",id:"9"},
			  {name:"恐怖片",id:"10"},
			  {name:"剧情片",id:"11"},
			  {name:"战争片",id:"12"}
		  ];
		  arr1=[
			  {name:"全部地区",data:""},
			  {name:"大陆",data:"大陆"},
			  {name:"美国",data:"美国"},
			  {name:"英国",data:"英国"},
			  {name:"日本",data:"日本"},
			  {name:"韩国",data:"韩国"},
			  {name:"泰国",data:"泰国"},
			  {name:"法国",data:"法国"},
			  {name:"印度",data:"印度"},
			  {name:"加拿大",data:"加拿大"},
			  {name:"香港",data:"香港"},
			  {name:"台湾",data:"台湾"},
			  {name:"海外",data:"海外"},
			  {name:"其他",data:"其他"}
		  ];
		  this.setData({
			  Type:arr,
			  Address:arr1,
			  type:6,
			  //初始化加载更多
			  page: 1,
			  isLastPage: false,
			  tips: '点击加载更多',
			  navList: []
		  })
		  this.getList();
	  }else if(data==2){//dsj
		  arr=[
		  		  {name:"国产剧",id:"13"},
		  		  {name:"港台剧",id:"14"},
		  		  {name:"日韩剧",id:"15"},
		  		  {name:"欧美剧",id:"16"},
		  			  
		  ];
		  this.setData({
			  Type:arr,
			  Address:k,
			  area:"",
			  address:"",
			  type:2,
			  //初始化加载更多
			  page: 1,
			  isLastPage: false,
			  tips: '点击加载更多',
			  navList: []
		  })
		  this.getList(); 
	  }else if(data==4){//dm
	  arr1=[
	  			  {name:"全部地区",data:""},
	  			  {name:"大陆",data:"大陆"},
	  			  {name:"美国",data:"美国"},
	  			  {name:"英国",data:"英国"},
	  			  {name:"日本",data:"日本"},
	  			  {name:"韩国",data:"韩国"},
	  			  {name:"泰国",data:"泰国"},
	  			  {name:"法国",data:"法国"},
	  			  {name:"印度",data:"印度"},
	  			  {name:"加拿大",data:"加拿大"},
	  			  {name:"香港",data:"香港"},
	  			  {name:"台湾",data:"台湾"},
	  			  {name:"海外",data:"海外"},
	  			  {name:"其他",data:"其他"}
	  ];
	  this.setData({
	  			  Type:k,
	  			  Address:arr1,
				  type:4,
				  address:"",
				  //初始化加载更多
				  page: 1,
				  isLastPage: false,
				  tips: '点击加载更多',
				  navList: []
	  })
	  this.getList();
		  
	  }else if(data==3){//zy
		  arr1=[
		  			  {name:"全部地区",data:""},
		  			  {name:"大陆",data:"大陆"},
		  			  {name:"美国",data:"美国"},
		  			  {name:"英国",data:"英国"},
		  			  {name:"日本",data:"日本"},
		  			  {name:"韩国",data:"韩国"},
		  			  {name:"泰国",data:"泰国"},
		  			  {name:"法国",data:"法国"},
		  			  {name:"印度",data:"印度"},
		  			  {name:"加拿大",data:"加拿大"},
		  			  {name:"香港",data:"香港"},
		  			  {name:"台湾",data:"台湾"},
		  			  {name:"海外",data:"海外"},
		  			  {name:"其他",data:"其他"}
		  ];
		  this.setData({
		  			  Type:k,
		  			  Address:arr1,
					  type:3,
					  address:"",
					  //初始化加载更多
					  page: 1,
					  isLastPage: false,
					  tips: '点击加载更多',
					  navList: []
		  })
		  this.getList();
		  
	  }
	  
  },
  type:function(e){
	  console.log(e)
	  let type=e.currentTarget.dataset.type;
	  this.setData({
		  ontype:e.target.dataset.index,
		  type:e.currentTarget.dataset.type,
		  //初始化加载更多
		  page: 1,
		  isLastPage: false,
		  tips: '点击加载更多',
		  navList: []
	  }) 
	  // console.log(this.data.type)
	  this.getList();
  },
  address:function(e){

	  this.setData({
	  		  area:e.currentTarget.dataset.address,
			  onaddress:e.target.dataset.index,
			  //初始化加载更多
			  page: 1,
			  isLastPage: false,
			  tips: '点击加载更多',
			  navList: []
	  })
	  this.getList();
  },
  
  
  
  showError: function () {
      wx.showToast({
          title: "网络异常",
          icon: 'loading'
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
	  this.getDataC();
	  this.getList();
	  //滑动隐藏显示
	  let that = this;
	  wx.createSelectorQuery().select('#fix-view').boundingClientRect(function(rect) {
	  //menuHeight = rect.bottom;
	  that.setData({
	  menuHeight: rect.bottom
	  })
	  }).exec()
	  
	  this.ad();
  },
  
  jiazai:function(e){
	  if (this.data.isLastPage) {
	             return
	         }
	         this.setData({ 
	  				page: this.data.page + 1 ,
	  				})
	   this.getList();
  },
  //滑动监听
  scroll(event){
     console.log(event)
	 
   },
   //触底-加载更多
  reactBottom() {
	this.jiazai();
    console.log('触底-加载更多');
  },
  // 插屏广告
  ad() {
  	// 在页面onLoad回调事件中创建插屏广告实例
  	if (wx.createInterstitialAd) {
  		interstitialAd = wx.createInterstitialAd({
  			adUnitId: 'adunit-e1b24a742e0c5fcd'
  		})
  		interstitialAd.onLoad(() => {})
  		interstitialAd.onError((err) => {})
  		interstitialAd.onClose(() => {})
  	}
  	setTimeout(function(){
  		// 在适合的场景显示插屏广告
  		if (interstitialAd) {
  			interstitialAd.show().catch((err) => {
  				console.error(err)
  			})
  		}
  	},1000)
  
  	
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
	  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
	  this.reactBottom();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //滑动隐藏显示
  onPageScroll: function(event) {
  	    let scroll = event.scrollTop; //当前的距离顶部的高度
  	    let scrollTop = this.data.scrollTop;  //记录的距离顶部的高度
  	    let height = this.data.menuHeight;  //菜单的高度
  	    let show = this.data.showMenu;  //菜单的显示状态
  	    //是否超过开始隐藏的高度
  	    if (scroll > height) {
  	      if ((scroll < scrollTop) == show) { //超过高度时的上滑或下滑状态一致时
  	        this.setData({
  	          scrollTop: scroll
  	        })
  	      } else { //超过高度时的上滑显示和下滑隐藏
  	        let anim = wx.createAnimation({
  	          timingFunction: 'ease-in-out',
  	          duration: 200,
  	          delay: 0
  	        })
  	        anim.translateY(scroll < scrollTop ? 0 : -height).step();
  	        this.setData({
  	          scrollTop: scroll,
  	          showMenu: scroll < scrollTop,
  	          menuAnim: anim.export()
  	        })
  	      }
  	    } else {
  	      //小于menuHeight并且隐藏时执行显示的动画
  	      if (!show) {
  	        let anim = wx.createAnimation({
  	          timingFunction: 'ease-in-out',
  	          duration: 200,
  	          delay: 0
  	        })
  	        anim.translateY(0).step();
  	        this.setData({
  	          scrollTop: scroll,
  	          showMenu: true,
  	          menuAnim: anim.export()
  	        })
  	      } else {
  	        this.setData({
  	          scrollTop: scroll
  	        })
  	      }
  	    }
  	  }
})