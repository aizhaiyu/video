//index.js
//获取应用实例
const app = getApp()

Page({
	data: {
		showModal: false,
		showDialog: false,
		dataC: null,
		login: false,

		User: '',
		Pass: '',

		r_user: '',
		r_pass: '',
		r_mail: '',


		userImg: "",
		userInfo: {},
		hasUserInfo: false,
		canIUse: wx.canIUse('button.open-type.getUserInfo')

	},

	//求片弹窗好的按钮
	okok() {
		this.setData({
			showModal: false
		})
	},

	//事件处理函数
	bindViewTap: function() {
		wx.showToast({
			title: "开发中",
			icon: "none",
			duration: 1500
		});
	},
	//数据
	getDataC() {
		let that = this;
		wx.request({
			url: app.globalData.Url + "public/?service=App.DataControl.User",
			data: {
				dataC: 1
			},
			success: (src) => {
				// console.log(src.data.Data.content)
				if (src.data.Code === 200) {
					that.setData({
						dataC: src.data.Data.content
					})
				} else {
					console.log("错误");
				}
			}
		})
	},

	//收藏
	collect: function() {
		// wx.navigateTo({
		// url: '/pages/collect/collect'
		// });
		wx.showToast({
			title: "开发中",
			icon: "none",
			duration: 1500
		});
	},
	//历史观看记录转跳
	history: function() {
		wx.navigateTo({
			url: '/pages/his/his'
		});
	},
	//我的收藏
	// collect:function(){
	// 	wx.showToast({
	// 				title:"开发中",
	// 				icon:"success",
	// 				duration:1000
	// 			});
	// }, 
	//添加
	kf: function() {
		this.setData({
			showModal: true
		});

		wx.showModal({
			title: '没有找到资源',
			content: '没有找到您需要的影片吗，可以加入qq交流群：155438612或关注公众号：爱宅域，发送影片名称可免费为您添加哦\n确定复制联系方式',
			success: function(res) {
				if (res.confirm) {
					//复制
					wx.setClipboardData({
						data: 'qq群：155438612 公众号:爱宅域',
						success: (res) => {
							wx.showToast({
								title: '复制成功'
							})
						}
					})
				} else {
					console.log('用户点击取消');
				}
			}
		});
	},
	//声明
	sm: function() {
		wx.showModal({
			title: '免责声明',
			content: '所有信息均来自网络，仅供个人学习，娱乐使用，任何人不得以非法或从事其他违法活动，本站不承担任何法律责任；如有侵犯您的合法权益，请联系我的邮箱，将会第一时间进行处理。邮箱：root@aizhaiyu.com\n确定复制邮箱',
			success: function(res) {
				if (res.confirm) {

					//复制
					wx.setClipboardData({
						data: "root@aizhaiyu.com",
						success: (res) => {
							wx.showToast({
								title: '复制成功'
							})
						}
					})
				} else {
					console.log('用户点击取消');
				}
			}
		});
	},
	//清除
	qc: function() {
		let that = this;
		wx.showModal({
			title: '注意',
			content: '清理本地数据缓存会将本地观看历史记录数据清除并退出登陆状态，是确定继续吗？',
			success: function(res) {
				if (res.confirm) {
					that.setData({
						login: false
					});
					wx.showToast({
						title: "清除成功",
						icon: "success",
						duration: 1000
					})
					try {
						wx.clearStorageSync()
						console.log("成功")
					} catch (e) {
						console.log(e)
					}
					console.log('用户点击确定');
				} else {
					console.log('用户点击取消');
				}
			}
		});

	},
	onLoad: function() {
		this.getDataC();
		// 登陆缓存读取 
		let hisimg = new Array();
		hisimg = wx.getStorageSync("userimg");
		// console.log(hisimg);
		if (hisimg == "") {
			console.log("无内容")

		} else {
			console.log("有内容")
			// console.log(hisimg)
			this.setData({
				// User:hisimg[0][0].user
				userImg: hisimg[0][0].img
			});
		}
		//
		let his = new Array();
		his = wx.getStorageSync("login");
		console.log(his);
		// console.log(his[0][0].code)
		if (his == "" || his[0][0].code == 1) {
			console.log("无内容")
			this.setData({
				login: false

			});
		} else {
			console.log("有内容")
			console.log(his)
			this.setData({
				login: true,
				user: his[0][0].name
			});
		}
		//获取头像信息 

		if (app.globalData.userInfo) {
			this.setData({
				userInfo: app.globalData.userInfo,
				hasUserInfo: true
			})
		} else if (this.data.canIUse) {
			// 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
			// 所以此处加入 callback 以防止这种情况
			app.userInfoReadyCallback = res => {
				this.setData({
					userInfo: res.userInfo,
					hasUserInfo: true
				})
			}
		} else {
			// 在没有 open-type=getUserInfo 版本的兼容处理
			wx.getUserInfo({
				success: res => {
					app.globalData.userInfo = res.userInfo
					this.setData({
						userInfo: res.userInfo,
						hasUserInfo: true
					})
				}
			})
		}
	},

	//登陆事件
	getLogin() {
		let that = this;
		var user1 = this.data.User;
		var pass1 = this.data.Pass;
		// console.log("账户："+this.data.User)
		// console.log("密码："+this.data.Pass)



		if (user1 == "" && pass1 == "") {
			wx.showToast({
				title: "请输入账号密码",
				icon: 'none',
				duration: 1500 //持续的时间
			})
		} else { //开始
			if (user1.length > 5 && pass1.length > 5) {
				//开始请求
				// console.log("账号："+user1)
				// console.log("密码："+pass1)
				wx.request({
					url: app.globalData.Url+"public/?service=App.User_Login.Go",
					data:{
						user:user1,
						pass:pass1
					},
					success: (res) => {
						console.log(res)

						if (res.data.Data.code == 1 && JSON.stringify(res.data.Data) == "{}") {

							//缓存登陆id
							let Login = new Array();
							Login = [{
								"code": 1,
								"id": "",
								"token": "",
								"login": false,
								"user_name": ""
							}];
							let arrL = wx.getStorageSync("login") || [];
							arrL.unshift(Login);
							wx.setStorageSync("login", arrL);

							console.log("失败")
							wx.showToast({
								title: res.data.Data.tips,
								icon: 'none',
								duration: 1500 //持续的时间
							});
							that.setData({
								login: false
							})
						} else {
							console.log("成功")
							//缓存登陆id
							let Login = new Array();
							Login = [{
								"code": res.data.Data.code,
								"id": res.data.Data.user_id,
								"token": res.data.Data.token,
								"login": true,
								"name": res.data.Data.user_name
							}];
							let arrL = wx.getStorageSync("login") || [];
							arrL.unshift(Login);
							wx.setStorageSync("login", arrL);

							wx.showToast({
								title: res.data.Data.tips,
								icon: 'none',
								duration: 1500 //持续的时间
							});
							that.setData({
								login: true,
								user: res.data.Data.user_name
							})
						}


					}
				})
				//请求结束

			} else {
				console.log("用户名密码长度至少需要大于等于6")
				wx.showToast({
					title: "账号或密码长度小于6",
					icon: 'none',
					duration: 1500 //持续的时间
				});
			}
			//
			//结束
		}



	},
	//登陆监听编辑框-----
	user: function(e) {
		// console.log(e.detail.value)
		this.data.User = e.detail.value;
	},
	pass: function(e1) {
		// console.log(e.detail.value)
		this.data.Pass = e1.detail.value;
		// console.log("密码："+this.data.Pass)
		// detail.value
	},
	//注册编辑框监听-----
	r_user: function(e) {
		// console.log(e.detail.value)
		this.data.r_user = e.detail.value;
	},
	r_pass: function(e) {
		// console.log(e.detail.value)
		this.data.r_pass = e.detail.value;

		// detail.value
	},
	r_mail: function(e) {
		// console.log(e.detail.value)
		this.data.r_mail = e.detail.value;
		// console.log("密码："+this.data.Pass)
		// detail.value
	},
	//注册事件
	register: function() {
		let that = this;
		let user = this.data.r_user;
		let pass = this.data.r_pass;
		let mail = this.data.r_mail;
		console.log("用户名：" + user.length)
		console.log("密码：" + pass.length)
		console.log("邮箱：" + mail.length)
		if (user == "" || pass == "" || mail == "") {
			// console.log("账号或密码或邮箱为空");
			wx.showToast({
				title: "账号或密码或邮箱为空",
				icon: 'none',
				duration: 1500 //持续的时间
			});
		} else if (user.length < 6 || pass.length < 6) {
			// console.log("账号或密码长度小于6");
			wx.showToast({
				title: "账号或密码长度小于6",
				icon: 'none',
				duration: 1500 //持续的时间
			});
		} else if (mail.length < 8) {
			wx.showToast({
				title: "邮箱长度小于8",
				icon: 'none',
				duration: 1500 //持续的时间
			});
		} else {
			// 正确开始
			console.log("输入格式正确");
			wx.request({
				url: app.globalData.Url+"public/?service=App.User_Register.Go",
					data:{
						user:user,
						pass:pass,
						mail:mail
					},
				success: (res) => {
					console.log(res);
					if (JSON.stringify(res.data.Data) == "{}") {
						wx.showToast({
							title: "注册失败，未知错误！",
							icon: 'none',
							duration: 1500 //持续的时间
						});
					} else {
						wx.showToast({
							title: "注册成功！",
							icon: 'none',
							duration: 1500 //持续的时间
						});
						that.toggleDialog();
					} //
				}
			})
			// 正确结束
		}
		//
	},
	//弹窗事件
	toggleDialog() {
		this.setData({
			showDialog: !this.data.showDialog
		});

	},
	getUserInfo: function(e) {
		// 缓存

		let userimg = new Array();
		userimg = [{
			"img": e.detail.userInfo.avatarUrl,
			"user": e.detail.userInfo.nickName
		}];
		let arrL = wx.getStorageSync("userimg") || [];
		arrL.unshift(userimg);
		wx.setStorageSync("userimg", arrL);
		//点击
		this.getLogin();

		console.log(e)

		app.globalData.userInfo = e.detail.userInfo
		this.setData({
			userImg: e.detail.userInfo.avatarUrl,
			userInfo: e.detail.userInfo,
			hasUserInfo: true
		});
	},
	////////////////////////保存图片
	//点击保存图片
	save() {
		let that = this

		//若二维码未加载完毕，加个动画提高用户体验
		wx.showToast({
			icon: 'loading',
			title: '正在保存图片',
			duration: 1000
		})
		//判断用户是否授权"保存到相册"
		wx.getSetting({
			success(res) {
				//没有权限，发起授权
				if (!res.authSetting['scope.writePhotosAlbum']) {
					wx.authorize({
						scope: 'scope.writePhotosAlbum',
						success() { //用户允许授权，保存图片到相册
							that.savePhoto();

						},
						fail() { //用户点击拒绝授权，跳转到设置页，引导用户授权
							wx.openSetting({
								success() {
									wx.authorize({
										scope: 'scope.writePhotosAlbum',
										success() {
											that.savePhoto();
										}
									})
								}
							})
						}
					})
				} else { //用户已授权，保存到相册
					that.savePhoto()
				}
			}
		})
	},
	//下载图片地址并保存到相册，提示保存成功
	savePhoto() {
		let that = this
		wx.downloadFile({
			// 图片下载地址
			url: 'https://ys.aizhaiyu.com/img/QR/aizhiayu.jpg',
			// url: app.apiUrl.url + 'Userequity/poster',
			success: function(res) {
				wx.saveImageToPhotosAlbum({
					filePath: res.tempFilePath,
					success(res) {
						wx.showToast({
							title: '保存成功',
							icon: "success",
							duration: 1000
						});
						that.setData({
							showModal: false
						});
					}
				})
			}
		})
	}
	////////////////////////保存图片
})
