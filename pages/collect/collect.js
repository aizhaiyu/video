// pages/collect/collect.js
// 在页面中定义插屏广告
let interstitialAd = null
Page({

  /**
   * 页面的初始数据
   */
  data: {
     list:[],
	 tis:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
	  this.ad();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
collect(){
	var collect=Array();
	collect=wx.getStorageSync("collect");
	if(collect.length==0){
		this.setData({
			tis:'- - 空空如也 - -'
		})
	}else{
		this.setData({
			tis:''
		})
	}
	this.setData({
		list:collect
	})

},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
	  this.collect();
  },
  /* 收藏清空 */
  Delete() {
  	var that = this;
  	wx.showModal({
  		title: "提示",
  		content: "是否清空全部收藏",
  		success: function(res) {
  			if (res.confirm) {
  				console.log("确定");
  				wx.removeStorageSync('collect');
  				wx.showToast({
  					title: '已清空',
  					icon: 'none',
  					duration: 2000 //持续的时间
  				})
  				that.setData({
  					tis: '空空如也'
  				})
  				that.collect();
  			} else {
  				console.log("取消")
  			}
  		}
  	})
  },
  // 插屏广告
  ad() {
  	// 在页面onLoad回调事件中创建插屏广告实例
  	if (wx.createInterstitialAd) {
  		interstitialAd = wx.createInterstitialAd({
  			adUnitId: 'adunit-54c48722f396d8fc'
  		})
  		interstitialAd.onLoad(() => {})
  		interstitialAd.onError((err) => {})
  		interstitialAd.onClose(() => {})
  	}
  	setTimeout(function(){
  		// 在适合的场景显示插屏广告
  		if (interstitialAd) {
  			interstitialAd.show().catch((err) => {
  				console.error(err)
  			})
  		}
  	},1000)
  
  	
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})