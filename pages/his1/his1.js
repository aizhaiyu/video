// pages/his1/his1.js
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		resVideo: [],
		tis: '最多保留30条记录' //
	},
	his() {
		var his = Array();
		his = wx.getStorageSync("his");
		var arr = Array();
		if (his.length == 0) {
			this.setData({
				tis: '空空如也'
			})
		} else {
			this.setData({
				tis: '最多保留30条记录'
			})
		}
		// console.log(his);
		for (var i in his) {
			let second = parseInt(his[i][0].time).toFixed(0); //秒
			let minute = 0; //分
			if (second >= 60) {
				minute = second / 60; //分
				second = second % 60;
			}
			// console.log(minute.toFixed(0)+'分'+second+'秒')
			his[i][0].time = minute.toFixed(0) + '分' + second + '秒';
		}
		console.log(his)
		this.setData({
			resVideo: his
		})
	},
	/* 历史记录清空 */
	hisDelete() {
		var that = this;
		wx.showModal({
			title: "提示",
			content: "是否清空播放记录",
			success: function(res) {
				if (res.confirm) {
					console.log("确定");
					wx.removeStorageSync('his');
					wx.showToast({
						title: '已清空',
						icon: 'none',
						duration: 2000 //持续的时间
					})
					that.setData({
						tis: '空空如也'
					})
					that.his();
				} else {

					console.log("取消")
				}
			}
		})
	},

	// 历史记录删除
	hisDel(e) {
		var id = e.currentTarget.dataset.id;
		var that = this;
		var his = Array();
		his = wx.getStorageSync("his");
		console.log(his)
		for (var i in his) {
			console.log(id == his[i][0].id)
			if (id == his[i][0].id) {
				his.splice(i, 1) //删除
				wx.setStorageSync("his", his); //赋值替换全部
				wx.showToast({
					title: "已删除",
					icon: 'none',
					duration: 1500 //持续的时间
				});
				that.his();
			}
		}



	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {
		this.his();
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
