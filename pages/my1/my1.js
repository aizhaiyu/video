const app = getApp();

Page({
	
  /**
   * 页面的初始数据
   */
  data: {
	  statusBarHeight: app.globalData.statusBarHeight,//状态栏高度
	  His:[],
	  ifHis:false,//判断是否存在历史
	  aiNum:0,
	  showModal:false,//按钮的显示或隐藏
	  myText:"相信奇迹的人本身就是奇迹"
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
	  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
	  this.his();
	  this.getData();
  },
  getData(){
	  var that=this;
	  wx.request({
	  		  url:app.globalData.Url+"public/?service=App.DataControl.User",
	  		  data:{
	  		  	dataC:1
	  		  },
	  		  success:(res)=>{
	  			  that.setData({
	  				  myText:res.data.Data.myText
	  			  })
	  		  }
	  })
  },
  /* 获取历史记录*/
  his(){
  	var his=Array();
  	his=wx.getStorageSync("his");
	var ai=Array();
	ai=wx.getStorageSync("collect");
	this.setData({
		aiNum:ai.length
	})
  	var arr=Array();
  	if(his.length==0){
  		this.setData({
  			ifHis:false
  		})
  	}else{
  		this.setData({
  			ifHis:true
  		})
  	}
  	// console.log(his);
  	for(let i in his){
  		let second=parseInt(his[i][0].time).toFixed(0);//秒
  		let minute=0;//分
  		if(second>=60){
  			minute=second/60;//分
  			second=second%60;
  		}
  		// console.log(minute.toFixed(0)+'分'+second+'秒')
  		his[i][0].time=minute.toFixed(0)+'分'+second+'秒';
  	}
  	console.log(his)
  	this.setData({
  		His:his
  	})
  },
  // 公众号ok
  okok:function(){
  	this.setData({
  		showModal:false
  	})
	//复制
	wx.setClipboardData({
		data: '爱宅域',
		success: (res) => {
			wx.showToast({
				title: '公众号昵称复制成功'
			})
		}
	})
  },
  /* 历史点击*/
  hisClick(){
	  wx.navigateTo({
	    url:'../his1/his1'
	  })
  },
  /* 收藏点击*/
  colClick(){
  	  wx.navigateTo({
  	    url:'../collect/collect'
  	  })
  },
  /* 关于点击*/
  gyClick(){
  	  wx.navigateTo({
  	    url:'../about/about'
  	  })
  },
  //添加
  kf: function() {
  	this.setData({
  		showModal: true
  	});
  
  	wx.showModal({
  		title: '没有找到资源？',
  		content: '没有找到您需要的影片吗，确定联系客服免费添加哦',
  		success: function(res) {
  			if (res.confirm) {
				var urrl=[];
				urrl="https://ys.aizhaiyu.com/img/QR/wx.jpg";
  				    wx.previewImage({
  				      current: urrl, // 当前显示图片的http链接
  				      urls: [urrl] // =============重点重点=============
  				    })

  			} else {
  				console.log('用户点击取消');
  			}
  		}
  	});
  },
  //赞赏
  zs(){
	  var urrl=[];
	  urrl="https://ys.aizhaiyu.com/img/QR/zsm.jpg";
	      wx.previewImage({
	        current: urrl, // 当前显示图片的http链接
	        urls: [urrl] // =============重点重点=============
	      })
  },
  //声明
  sm: function() {
  	wx.showModal({
  		title: '免责声明',
  		content: '所有信息均来自网络，仅供个人学习，娱乐使用，任何人不得以非法或从事其他违法活动，本站不承担任何法律责任；如有侵犯您的合法权益，请联系邮箱，我们将会第一时间进行处理。邮箱：root@aizhaiyu.com\n确定复制邮箱',
  		success: function(res) {
  			if (res.confirm) {
  
  				//复制
  				wx.setClipboardData({
  					data: "root@aizhaiyu.com",
  					success: (res) => {
  						wx.showToast({
  							title: '复制成功'
  						})
  					}
  				})
  			} else {
  				console.log('用户点击取消');
  			}
  		}
  	});
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  ////////////////////////保存图片
  //点击保存图片
  
  save(){
	  var urrl=[];
	  urrl="https://ys.aizhaiyu.com/img/QR/aizhaiyu.jpg";
	      wx.previewImage({
	        current: urrl, // 当前显示图片的http链接
	        urls: [urrl] // =============重点重点=============
	      })
		  
  	let that = this
  
  	//若二维码未加载完毕，加个动画提高用户体验
  	wx.showToast({
  		icon: 'loading',
  		title: '正在保存图片',
  		duration: 1000
  	})
  	//判断用户是否授权"保存到相册"
  	wx.getSetting({
  		success(res) {
  			//没有权限，发起授权
  			if (!res.authSetting['scope.writePhotosAlbum']) {
  				wx.authorize({
  					scope: 'scope.writePhotosAlbum',
  					success() { //用户允许授权，保存图片到相册
  						that.savePhoto();
  
  					},
  					fail() { //用户点击拒绝授权，跳转到设置页，引导用户授权
  						wx.openSetting({
  							success() {
  								wx.authorize({
  									scope: 'scope.writePhotosAlbum',
  									success() {
  										that.savePhoto();
  									}
  								})
  							}
  						})
  					}
  				})
  			} else { //用户已授权，保存到相册
  				that.savePhoto()
  			}
  		}
  	})
  },
  //下载图片地址并保存到相册，提示保存成功
  savePhoto() {
  	let that = this
  	wx.downloadFile({
  		// 图片下载地址
  		url: 'https://ys.aizhaiyu.com/img/QR/aizhiayu.jpg',
  		// url: app.apiUrl.url + 'Userequity/poster',
  		success: function(res) {
  			wx.saveImageToPhotosAlbum({
  				filePath: res.tempFilePath,
  				success(res) {
  					wx.showToast({
  						title: '保存成功',
  						icon: "success",
  						duration: 1000
  					});
  					that.setData({
  						showModal: false
  					});
  				}
  			})
  		}
  	})
  },
  ////////////////////////保存图片
})