// pages/search/search.js
const app = getApp()
// 在页面中定义插屏广告
let interstitialAd = null
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		dataC: null,
		rcm: [],
		searchText: "",
		iF: false,
		list: [],
		page: 1,
		context: false, //没有内容则显示
		his: false,
		His: [],
		hisText: "",
		sea: false
	},


	//数据
	getDataC() {
		let that = this;
		wx.request({
			url: app.globalData.Url + "public/?service=App.DataControl.User",
			data: {
				dataC: 1
			},
			success: (src) => {
				console.log(src.data.Data.content)
				if (src.data.Code === 200) {
					that.setData({
						dataC: src.data.Data.content
					})
				} else {
					console.log("错误");
				}
			}
		})
	},
	/* 
	获取搜索历史
	*/

	/* 
	 获取最新推荐关键词
	 */
	getRce() {
		let that = this;
		wx.request({
			url: app.globalData.Url + "public/?service=App.Mov.GetSearchRec",
			success: (res) => {
				console.log(res);
				that.setData({
					rcm: res.data.Data
				})
			}
		})
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		this.getRce();
		this.getDataC();
		this.getHis(); //获取搜索缓存
		//缓冲提醒
		wx.showToast({
			title: '加载中',
			icon: 'loading',
			duration: 400
		})
		
		this.ad();
	},
	hisDel(e) {
		var id = e.currentTarget.dataset.id;
		var that = this;
		var his = Array();
		his = wx.getStorageSync("search");
		console.log(his)
		for (var i in his) {
			console.log(id == his[i][0].id)
			if (id == his[i][0].id) {
				his.splice(i, 1) //删除
				wx.setStorageSync("search", his); //赋值替换全部
				wx.showToast({
					title: "已删除",
					icon: 'none',
					duration: 1500 //持续的时间
				});
				that.getHis();
			}
		}
	},
	/* 获取搜索缓存*/
	getHis: function() {
		let sea = Array();
		sea = wx.getStorageSync("search");
		console.log(sea); //读取搜索缓存
		if (sea.length == 0) {
			this.setData({
				his: false
			})
		} else {
			this.setData({
				His: sea,
				his: true
			})
		}
		//首次提示教程
		let his1 = Array();
		his1 = wx.getStorageSync("searchhis");
		if (his1.length == 0) {
			this.setData({
				sea: true
			})
		} else {
			this.setData({
				sea: false
			})
		}
	},
	/*
	 搜索事件
	 */
	getSearch(page, value) {
		let that = this;
		wx.request({
			url: app.globalData.Url + "public/?service=App.Mov.SearchVod",
			data: {
				key: value,
				limit: 20,
				page: page
			},
			success: (src) => {
				// console.log(src.data.Data);
				//判断内容
				if (src.data.Data == undefined || JSON.stringify(src.data.Data) == "{}") {
					// console.log("空")
					that.setData({
						context: true
					})
				} else {
					// console.log("非空")
					that.setData({
						context: false
					})
				}
				that.setData({
					list: src.data.Data
				})
			}
		})
	},
	/* 
	 搜索监听
	 */
	searchE: function(e) {
		// console.log(e);
		let value = e.detail.value; //输入内容
		this.getSearch(1, value);
		this.data.hisText = value;
		// console.log(this.data.hisText)
		if (value == "") { //判断无内容则显示
			this.getHis();
			this.setData({
				iF: false
			})
		} else {
			//显示搜索内容
			this.setData({
				iF: true
			})

		}
	},
	/* 
	 历史搜索
	 */
	ssx: function() {
		wx.setStorageSync("searchhis", "ssx");
		this.setData({
			sea: false
		})
	},
	/* 缓存历史搜索*/
	hiss: function() {
		if (this.data.hisText == "") {
			console.log("空")
			wx.showToast({
				title: "请输入片名",
				icon: 'none',
				duration: 1500 //持续的时间
			});
		} else {
			wx.showToast({
				title: "已添加到搜索历史",
				icon: 'none',
				duration: 1500 //持续的时间
			});
			console.log("不空")
			let ss = this.data.hisText;
			console.log(ss);
			let sea = Array();
			sea = wx.getStorageSync("search");
			console.log(sea); //读取搜索缓存

			for (let i = 0, j = 5; i <= sea.length; i++) {
				if (ss == sea[i]) {
					sea.splice(i, 1);
				}
				if (sea.length > j) { //
					sea.splice(sea.length - 1, 1)
				}
			}
			wx.setStorageSync("search", sea);
			// 缓存多条数据
			let arrH = wx.getStorageSync("search") || [];
			arrH.unshift(ss);
			wx.setStorageSync("search", arrH);
			this.setData({
				his: true
			});
			// this.getHis();
		}
	},
	/* 历史记录删除 */
	hisDelete() {
		var that = this;
		wx.showModal({
			title: "提示",
			content: "是否清空搜索记录",
			success: function(res) {
				if (res.confirm) {
					console.log("确定");
					wx.removeStorageSync('search');
					wx.showToast({
						title: '已清空',
						icon: 'none',
						duration: 2000 //持续的时间
					})
					that.setData({
						his: false
					})
				} else {

					console.log("取消")
				}
			}
		})

	},
	/* 
	 关键词搜索
	 */
	searchTop: function(e) {
		// console.log(e.currentTarget.dataset.name)

		let name = e.currentTarget.dataset.name;

		// console.log(name);
		this.setData({
			//赋值名称
			searchText: name
		})


		if (this.iF == true) { //判断
			console.log("true")
			this.setData({
				iF: false
			})

		} else {
			//显示搜索内容
			console.log("false")
			this.setData({
				iF: true
			})

		}
		this.getSearch(1, name);
	},
	/* 
	 上下页
	 */
	setPageS: function(e) {
		this.setData({
			page: page - 1
		})
	},
	/* 
	 联系复制
	 */
	copy: function(e) {
		let that = this;
		//复制
		wx.setClipboardData({
			data: "公众号:爱宅域,回复影视加群 或 加微信邀进群：aizhaiyu",
			success: (res) => {
				wx.showToast({
					title: '复制成功'
				})
			}
		})
	},
	// 插屏广告
	ad() {
		// 在页面onLoad回调事件中创建插屏广告实例
		if (wx.createInterstitialAd) {
			interstitialAd = wx.createInterstitialAd({
				adUnitId: 'adunit-67c587604e33bb48'
			})
			interstitialAd.onLoad(() => {})
			interstitialAd.onError((err) => {})
			interstitialAd.onClose(() => {})
		}
		setTimeout(function(){
			// 在适合的场景显示插屏广告
			if (interstitialAd) {
				interstitialAd.show().catch((err) => {
					console.error(err)
				})
			}
		},1000)

		
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
