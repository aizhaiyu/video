const app = getApp();
Page({
	

	/**
	 * 页面的初始数据
	 */
	data: {
		navHeight: app.navHeight,
		cardCur: 0,//轮播图
		//跑马灯
			//跑马灯
		userInfo: {},//
		dataC:true,
		ggkz:null,
		gg:"",
		ggok:"确定",
		ggno:"取消",
		navH: 0,
		Title: "首页",
		//首页轮播图
		navImg:[],
		//首页推荐视频
		recVideoId:[],//电影
		recVideoId2:[],//电视剧
		recVideoId3:[],//综艺
		recVideoId4:[],//动漫
		showModal:false,//按钮的显示或隐藏
	},
	
	// 公众号ok
	okok:function(){
		this.setData({
			showModal:false
		})
	},
	/* 关于点击*/
	gyClick(){
		  wx.navigateTo({
		    url:'../about/about'
		  })
	},

	// 首页导航按钮
	//历史观看记录转跳
	// history: function() {
	// 	wx.switchTab({
	// 		url: '/pages/his/his'
	// 	});
	// },

	//清除
	// qc: function() {
	// 	let that = this;
	// 	wx.showModal({
	// 		title: '注意',
	// 		content: '清理本地数据缓存会将本地观看历史记录数据清除并退出登陆状态，是确定继续吗？',
	// 		success: function(res) {
	// 			if (res.confirm) {
	// 				that.setData({
	// 					login: false
	// 				});
	// 				wx.showToast({
	// 					title: "清除成功",
	// 					icon: "success",
	// 					duration: 1000
	// 				})
	// 				try {
	// 					wx.clearStorageSync()
	// 					console.log("成功")
	// 				} catch (e) {
	// 					console.log(e)
	// 				}
	// 				console.log('用户点击确定');
	// 			} else {
	// 				console.log('用户点击取消');
	// 			}
	// 		}
	// 	});
	
	// },
	
	
	
	
	
	//数据
	getDataC(){
		let that=this;
		wx.request({
			url:app.globalData.Url+"public/?service=App.DataControl.User",
			data:{
				dataC:1
			},
			success:(src)=>{
				console.log(src)
				if(src.data.Code===200){
					
					// if(src.data.Data.content!=true){
					// 	wx.redirectTo({
					// 		url: '/pages/gojuuonn/gojuuonn'
					// 	});
					// }
					
					//
					
					that.setData({
						dataC:src.data.Data.content,
						ggkz:src.data.Data.ggkz,
						gg:src.data.Data.gg,
						ggok:src.data.Data.ggok,
						ggno:src.data.Data.ggno
					})
					if(src.data.Data.ggtc===true){
						//弹出
						wx.showToast({
							title: "加载中...",
							icon: "none",
							duration: 1500
						});
						console.log(that)
						that.popup.showPopup();
					}
				}else{
					console.log("错误"); 
				} 
			}
		})
	},
	// 首页轮播图
	getNavImg(){
		let thas=this;
		wx.request({
			url:app.globalData.Url+"public/?service=App.Mov.GetHomeLevelAll",
			data:{
				level:9,
				page:1,
				limit:5
			},
			success: (scr) => {
				thas.setData({
					navImg:scr.data.Data
				})
			}
		})
	},
	//获取首页推荐视频 
	getVideo(){//电影
		let thas=this;
		wx.request({
			url:app.globalData.Url+"public/?service=App.Mov.GetHomeLevelAll",
			data:{
				level:1,
				page:1,
				limit:6
			},
			success: (res) => {
				thas.setData({
					recVideoId:res.data.Data
				});
			}
		})
	},
	getVideo2(){//电视剧
		let thas=this;
		wx.request({
			url:app.globalData.Url+"public/?service=App.Mov.GetHomeLevelAll",
			data:{
				level:2,
				page:1,
				limit:6
			},
			success: (res) => {
				thas.setData({ 
					recVideoId2:res.data.Data
				})
			}
		})
	},
	getVideo3(){//综艺
		let thas=this;
		wx.request({
			url:app.globalData.Url+"public/?service=App.Mov.GetHomeLevelAll",
			data:{
				level:3,
				page:1,
				limit:6
			},
			success: (res) => {
				thas.setData({
					recVideoId3:res.data.Data
				})
			}
		}) 
	},
	getVideo4(){//动漫
		let thas=this;
		wx.request({
			url:app.globalData.Url+"public/?service=App.Mov.GetHomeLevelAll",
			data:{
				level:4,
				page:1,
				limit:6
			},
			success: (res) => {
				thas.setData({
					recVideoId4:res.data.Data
				})
			}
		})
	},
	/* 公告弹窗 */
	/* 弹窗广告事件 */
		  _error:function() {
		    console.log('点击了取消');
		    this.popup.hidePopup();
		  },
		  //确认事件
		  _success:function() {
		    console.log('点击了确定');
		    this.popup.hidePopup();
		  },
		/* 五十 */
	  goGojuuonn:function(){
		  
	    wx.navigateTo({
	      url:'../gojuuonn/gojuuonn'
	    })
	  },
	  // 转跳
	  turn(){
	  	wx.navigateTo({
	  	  url:'../video/video'
	  	})
	  },
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
	
		wx.showShareMenu({
			withShareTicket: true,
			menus: ['shareAppMessage', 'shareTimeline'],
		}) //分享
		//获得popup公告弹窗组件
		  this.popup = this.selectComponent("#popup");
		  
		this.getDataC();
		this.getNavImg();
		this.getVideo();//1
		this.getVideo2();//2
		this.getVideo3();//3
		this.getVideo4();//4
		//轮播图

		
	},
	// 加载框
	loadModal() {
	    this.setData({
	      loadModal: true
	    })
	    setTimeout(()=> {
	      this.setData({
	        loadModal: false
	      })
	    }, 2000)
	  },
	/* 
	轮播图 
	 */
	// cardSwiper
	cardSwiper(e) {
	  this.setData({
	    cardCur: e.detail.current
	  })
	},
	
	

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

		// //获得popup公告弹窗组件
		//   this.popup = this.selectComponent("#popup");
		  // let that=this;
		  // console.log("弹出"+this.data.ggtc)
		  // if(that.ggtc==true){
			 //  //弹出
			 //  console.log("弹出")
			 //  this.popup.showPopup();
		  // }
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},


	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {
		return {
			title:"看视频就用我，VIP电影电视剧动漫全免费看( •̀ ω •́ )✧抢先看 - 爱宅域影视",
			desc: '能白嫖绝不充钱，难道不是吗',
			imageUrl:"http://ys.aizhaiyu.com/img/cover/main.jpg"
		}
	},
	//朋友圈
	onShareTimeline:function(){
		return{
			title:"看视频就用我，VIP电影电视剧动漫全免费看 - 爱宅域影视"
		}
	},
	
	

	
})
