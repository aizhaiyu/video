// pages/about/about.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
	  message:"暂无内容"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
	  this.getMessage();
  },
  getMessage(){
	  var that=this;
	  wx.request({
		  url:app.globalData.Url+"public/?service=App.DataControl.User",
		  data:{
		  	dataC:1
		  },
		  success:(res)=>{
			  that.setData({
				  message:res.data.Data.message
			  })
		  }
	  })
  },
  gzh(){
	  wx.setClipboardData({
	  	data: '爱宅域',
	  	success: (res) => {
	  		wx.showToast({
	  			title: '公众号昵称复制成功'
	  		})
	  	}
	  })
  },
  wx(){
	  wx.setClipboardData({
	  	data: 'aizhaiyu',
	  	success: (res) => {
	  		wx.showToast({
	  			title: '微信号复制成功'
	  		})
	  	}
	  })
  },
  yx(){
	  wx.setClipboardData({
	  	data: 'root@aizhaiyu.com',
	  	success: (res) => {
	  		wx.showToast({
	  			title: '邮箱地址复制成功'
	  		})
	  	}
	  })
  },
  gw(){
	  wx.setClipboardData({
	  	data: 'www.aizhaiyu.com',
	  	success: (res) => {
	  		wx.showToast({
	  			title: '网站地址复制成功'
	  		})
	  	}
	  })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})