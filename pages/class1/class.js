// pages/class1/class.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
	  tab_index: 0,
	  navList: [],
	  navList1: [],//1
	  navList2: [],
	  navList3: [],
	  
	  listView:[],
	  //筛选
	  type:7,
	  type1:2,
	  area:"",
	  // year:null,
	  sort:1,
	  page:1,
	  limit:18,
	  
	  
	  //筛选数组赋值
	  Sort:[
		  {name:"最新",id:"1"},
		  {name:"评分",id:"2"},
		  {name:"热度",id:"3"}
	  ],
	  Address:[
		  {name:"大陆"},
		  {name:"美国"},
		  {name:"英国"},
		  {name:"日本"},
		  {name:"韩国"},
		  {name:"泰国"},
		  {name:"法国"},
		  {name:"印度"},
		  {name:"海外"},
		  {name:"其他"}
	  ],
	  Type:[
		  {name:"动作片",id:"6"},
		  {name:"喜剧片",id:"7"},
		  {name:"爱情片",id:"8"},
		  {name:"科幻片",id:"9"},
		  {name:"恐怖片",id:"10"},
		  {name:"剧情片",id:"11"},
		  {name:"战争片",id:"12"}
	  ],
	  //电视剧
	  Type1:[
	  		  {name:"国产剧",id:"6"},
	  		  {name:"港台剧",id:"7"},
	  		  {name:"日韩剧",id:"8"},
	  		  {name:"欧美剧",id:"9"},
			  
	  ]
	  
  },

  /* 1点击*/
  type:function(e){
	  this.setData({
		  type:e.currentTarget.dataset.type
	  })
	  // console.log(this.data.type)
	  this.getNavList();
  },
  address:function(e){
	  this.setData({
	  		  area:e.currentTarget.dataset.address
	  })
	  this.getNavList();
  },
  /* 1*/
  type1:function(e){
  	  this.setData({
  		  type1:e.currentTarget.dataset.type1
  	  })
  	  // console.log(this.data.type)
  	  this.getNavList();
  },
  address1:function(e){
	  this.setData({
	  		  area1:e.currentTarget.dataset.address
	  })
	  this.getNavList();
  },
/* 获取影片 */
//列表加载
//type,area,year,sort,page,limit
	getNavList() {
		let that = this;
		// console.log(this.page)
		wx.request({
			url: "https://ys.aizhaiyu.com/app/cms/public/?service=App.Mov.GetCategory&type="+that.data.type+"&area="+that.data.area+"&year=$sort="+that.data.sort+"&page="+that.data.page+"&limit="+that.data.limit,
			success: (res) => {
				console.log(res)
				that.setData({
					navList: res.data.Data,
					
				})
			}
		})
	},
// 电视剧
	getNavList1() {
		let that = this;
		wx.request({
			url: "https://ys.aizhaiyu.com/app/cms/public/?service=App.Mov.GetCategory&type="+that.data.type1+"&area="+that.data.area1+"&year=$sort="+that.data.sort+"&page="+that.data.page+"&limit="+that.data.limit,
			success: (res) => {
				console.log(res)
				that.setData({
					navList1: res.data.Data,
					
				})
			}
		})
	},
	
	
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
	  this.systemType();
	  this.getNavList();
  },
  
   scroll (event) {
      console.log(event)
    },
   
    reactBottom () {
      console.log('触底-加载更多');
	  let that=this;
	  console.log(that)
    },
// 获取设备屏幕高度
  systemType () {
    wx.getSystemInfo({
      success: (res) => {
        let windowHeight = res.windowHeight
 
        this.setData({
          windowHeight: windowHeight
        })
 
        console.log(res)
      }
    })
  },
 
  tabChange (event) {
    this.setData({
      tab_index: event.detail.current
    })
  },
 
  // tab栏选择
  selectTab (event) {
    this.setData({
      tab_index: event.currentTarget.dataset.index
    })
  },
 
  // 返回顶部
  backTop () {
    let tab_index = this.data.tab_index
 
    this.setData({
      ['scrollTop' + tab_index]: 0
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})